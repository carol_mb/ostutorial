/*
g++ -std=c++11 data_process.cpp -o dp
./dp
*/

#include <iostream>
#include <string>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::stof;
using std::string;
using std::getline;

int main() {
	string line;
	std::ifstream myfile ("results.txt");
	string files[4] = {"to_graph_0.txt", "to_graph_1.txt", "to_graph_2.txt", "to_graph_3.txt"};
	if (myfile.is_open()) {
		for(int i = 0; i < 4; i++){
			std::ofstream output (files[i]);
			getline(myfile, line);
			float value = stof(line);
			output << 0 << " \"one thread\" " <<value << endl;

			value = 0;
			getline(myfile, line);
			value = stof(line);
			getline(myfile, line);
			value += stof(line);
			value /= 2;
			output << 1 << " \"two threads two cores\" "<< value << endl;

			value = 0;
			getline(myfile, line);
			value = stof(line);
			getline(myfile, line);
			value += stof(line);
			value /= 2;
			output << 2 << " \"two threads one core\" " << value << endl;
			output.close();
		}
		myfile.close();
	} else {
		cout << "Unable to open file"; 
	}
	return 0;
}