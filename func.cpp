/**
	g++ -std=c++11 -pthread func.cpp -o func
	./func > results.txt
**/
#define MAX 100000000

#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include <chrono>
#include <sched.h>
#include <pthread.h>
#include <thread>
#include <ratio>
#include <ctime>
#include <errno.h>

using std::vector;
using std::rand;
using std::srand;
using std::iterator;
using std::sin;
using std::thread;
using namespace std::chrono;


float generate_float() {
	float r = rand() % 1000000;
	int s = rand() % 100;
	r = r/999999;
	r = s > 50 ? (-1)*r : r; 
	return r;
}

float sum_all(vector<float> floats) {
	float sum = 0;
	for(vector<float>::iterator it = floats.begin(); it != floats.end(); it++) {
		sum += *it;
	}
	return sum;
}

float sum_sin(vector<float> floats) {
	float sum = 0;
	for(vector<float>::iterator it = floats.begin(); it != floats.end(); it++) {
		sum += sin(*it);
	}
	return sum;	
}

float sum_log(vector<float> floats) {
	float sum = 0;
	for(vector<float>::iterator it = floats.begin(); it != floats.end(); it++) {
		sum += log(*it);
	}
	return sum;	
}

vector<float> inicialize() {
	vector<float> floats;
	srand(time(NULL));
	float rand_number;
	for(int i = 0; i < MAX; i++){
		rand_number = generate_float();
		floats.push_back(rand_number);
	}
	return floats;
}
//---------------------------------------------------------------------------------------
void ini_time() {
    auto t1 = std::chrono::high_resolution_clock::now();
	
    inicialize();

    auto t2 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> t = t2 - t1;
    std::cout << t.count() << std::endl;
}

void sum_all_time(vector<float> floats) {
    auto t1 = std::chrono::high_resolution_clock::now();
	
    sum_all(floats);

    auto t2 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> t = t2 - t1;
    std::cout << t.count() << std::endl;
}

void sum_sin_time(vector<float> floats) {
    auto t1 = high_resolution_clock::now();
	
    sum_sin(floats);

    auto t2 = high_resolution_clock::now();
    std::chrono::duration<double> t = t2 - t1;
    std::cout << t.count() << std::endl;
}

void sum_log_time(vector<float> floats) {
    auto t1 = high_resolution_clock::now();
	
    sum_log(floats);

    auto t2 = high_resolution_clock::now();
    std::chrono::duration<double> t = t2 - t1;
    std::cout << t.count() << std::endl;
}
//---------------------------------------------------------------------------------------
void single_thread() {
	thread t(ini_time);
    cpu_set_t cpuset;
 
    CPU_ZERO(&cpuset);
    CPU_SET(0, &cpuset);
    pthread_setaffinity_np(t.native_handle(), sizeof(cpu_set_t), &cpuset);
 
    t.join();
}

void two_cores() {
	thread t1(ini_time);
    thread t2(ini_time);
    cpu_set_t cpuset1, cpuset2;
 
    CPU_ZERO(&cpuset1);
    CPU_SET(1, &cpuset1);
    pthread_setaffinity_np(t1.native_handle(), sizeof(cpu_set_t), &cpuset1);
    
	CPU_ZERO(&cpuset2);
	CPU_SET(2, &cpuset2);
    pthread_setaffinity_np(t2.native_handle(), sizeof(cpu_set_t), &cpuset2); 
    
    t1.join();
    t2.join();
}

void one_core() {
	thread t1(ini_time);
    thread t2(ini_time);
    cpu_set_t cpuset;
 
    CPU_ZERO(&cpuset);
    CPU_SET(3, &cpuset);
    
    pthread_setaffinity_np(t1.native_handle(), sizeof(cpu_set_t), &cpuset);   
    pthread_setaffinity_np(t2.native_handle(), sizeof(cpu_set_t), &cpuset); 

    t1.join();
    t2.join();
}
//---------------------------------------------------------------------------------------
/*
com argumentos
*/
void single_thread(void (*func)(vector<float>), vector<float> v) {
	thread t(func, v);
    cpu_set_t cpuset;
 
    CPU_ZERO(&cpuset);
    CPU_SET(0, &cpuset);
    pthread_setaffinity_np(t.native_handle(), sizeof(cpu_set_t), &cpuset);
    t.join();
}

void two_cores(void (*func)(vector<float>), vector<float> v) {
	thread t1(func, v);
    thread t2(func, v);
    cpu_set_t cpuset;
 
    CPU_ZERO(&cpuset);
    CPU_SET(1, &cpuset);
    pthread_setaffinity_np(t1.native_handle(), sizeof(cpu_set_t), &cpuset);
 	
 	CPU_ZERO(&cpuset);
 	CPU_SET(2, &cpuset);
    pthread_setaffinity_np(t2.native_handle(), sizeof(cpu_set_t), &cpuset); 

    t1.join();
    t2.join();
}

void one_core(void (*func)(vector<float>), vector<float> v) {
	thread t1(func, v);
    thread t2(func, v);
    cpu_set_t cpuset;
 
    CPU_ZERO(&cpuset);
    CPU_SET(3, &cpuset);
    pthread_setaffinity_np(t1.native_handle(), sizeof(cpu_set_t), &cpuset);
    pthread_setaffinity_np(t2.native_handle(), sizeof(cpu_set_t), &cpuset); 

    t1.join();
    t2.join();
}
//---------------------------------------------------------------------------------------
/*
testes para ini_time
*/
void general_ini_time() {
	single_thread();
	two_cores();
	one_core();
} 

/*
testes para sum_all_time
*/
void general_sum_time() {
	vector<float> floats = inicialize();
	single_thread(sum_all_time, floats);
	two_cores(sum_all_time, floats);
	one_core(sum_all_time, floats);
}

/*
testes para sum_sin_time
*/
void general_sum_sin_time() {
	vector<float> floats = inicialize();
	single_thread(sum_sin_time, floats);
	two_cores(sum_sin_time, floats);
	one_core(sum_sin_time, floats);
}

/*
testes para sum_log_time
*/
void general_sum_log_time() {
	vector<float> floats = inicialize();
	single_thread(sum_log_time, floats);
	two_cores(sum_log_time, floats);
	one_core(sum_log_time, floats);
}

int main() {
	general_ini_time();
	general_sum_time();
	general_sum_sin_time();
	general_sum_log_time();
	return 0;
}