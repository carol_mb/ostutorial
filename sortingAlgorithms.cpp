#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#define MIN_SIZE 5000
#define MAX_SIZE 160000
using namespace std;

void print(int *v, int size){
	for(int i = 0; i < size; i++){
		cout << v[i] << " ";
	}
	cout << endl;
}
//------------------------------------------------------------
void swap(int *v, int a, int b){
	int temp = v[a];
	v[a] = v[b];
	v[b] = temp;
}
//------------------------------------------------------------
void bubble_sort(int *v, int size){
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size-1; j++){
			if(v[j] > v[j+1]){
				swap(v, j, j+1);
			}
		}
	}
}
//------------------------------------------------------------
/**
*Fonte: https://pt.wikipedia.org/wiki/Quicksort
**/
void quick_sort(int *v, int left, int right){
    int pivot = left, i, ch, j;
    for(i = left+1; i <= right; i++){
        j = i;
        if(v[j] < v[pivot]){
        	ch = v[j];
         	while(j > pivot){
            	v[j] = v[j-1];
            	j--;
         	}
         	v[j] = ch;
         	pivot++;
        }
    }
    if(pivot - 1 > left){
    	quick_sort(v, left, pivot-1);
    }
    if(pivot + 1 < right){
        quick_sort(v, pivot+1, right);
    }
}
//------------------------------------------------------------
void down(int *v, int i, int n){
	int j;
	while(2*i < n){
		j = 2*i;
		if(j+1 < n){
			if(v[j+1] > v[j])
				j++;
		}
		if(v[i] < v[j]){
			swap(v, i, j);
			i = j;
		} else {
			break;
		}
	}
}

void create_heap(int *v, int n){
	for(int i = n/2; i >= 0; i--){
		down(v, i, n);
	}
}

void heap_sort(int *v, int size){
	create_heap(v, size);
	for(int s = size-1; s > 1;){
		swap(v, 0, s);
		s--;
		down(v, 0, s);
	}
}
//------------------------------------------------------------
int* create_vector(int size, int seed){
	int *v = new int[size];
	srand(seed);
	for(int i = 0; i < size; i++){
		v[i] = rand();
	}
	return v;
}
//------------------------------------------------------------
void delete_vector(int *v, int size){
	delete[] v;
}
//------------------------------------------------------------
void time_test(){

    ofstream results;
    results.open ("time_test.dat");

    time_t bubble_result, quick_result, heap_result;

	for(int i = MIN_SIZE; i <= MAX_SIZE; i += 5000) {
        results << i << endl;
		for(int j = 1; j <= 10; j++){
			int *v0 = create_vector(i, j); //preencher o vetor
			int *v1 = create_vector(i, j);
			int *v2 = create_vector(i, j);

			bubble_result = time(NULL);
            bubble_sort(v0, i);
            bubble_result = time(NULL) - bubble_result;

            quick_result = time(NULL);
            quick_sort(v1, 0, i-1);
            quick_result = time(NULL) - quick_result;

            heap_result = time(NULL);
            heap_sort(v2, i);
            heap_result = time(NULL) - heap_result;

            results << bubble_result << " "
                    << quick_result << " "
                    << heap_result << endl;

			delete_vector(v0, i);
			delete_vector(v1, i);
			delete_vector(v2, i);
		}
	}
}
//------------------------------------------------------------
void clock_test(){

    ofstream results;
    results.open ("clock_test.dat");

    clock_t bubble_result, quick_result, heap_result;

	for(int i = MIN_SIZE; i <= MAX_SIZE; i += 5000) {
        results << i << endl;
		for(int j = 1; j <= 10; j++){
			int *v0 = create_vector(i, j); //preencher o vetor
			int *v1 = create_vector(i, j);
			int *v2 = create_vector(i, j);

			bubble_result = clock();
            bubble_sort(v0, i);
            bubble_result = clock() - bubble_result;

            quick_result = clock();
            quick_sort(v1, 0, i-1);
            quick_result = clock() - quick_result;

            heap_result = clock();
            heap_sort(v2, i);
            heap_result = clock() - heap_result;

            results << bubble_result << " "
                    << quick_result << " "
                    << heap_result << endl;

			delete_vector(v0, i);
			delete_vector(v1, i);
			delete_vector(v2, i);
		}
	}
}

double mean(int v[10]) {
    double sum = 0;
    for(int i = 0; i < 10; i++) {
        sum += v[i];
    }
    return sum / 10;
}

double deviation(int v[10], double mean) {
    double sum = 0;
    for(int i = 0; i < 10; i++) {
        sum += (v[i] - mean) * (v[i] - mean);
    }
    sum /= 10;
    return sqrt(sum);
}

void calculate_statistics(string infile, string outfile) {
    ifstream results(infile + ".dat");

    if (results.is_open()) {
        ofstream statistics_bubble;
        ofstream statistics_quick;
        ofstream statistics_heap;
        statistics_bubble.open(outfile + "_bubble.dat");
        statistics_quick.open(outfile + "_quick.dat");
        statistics_heap.open(outfile + "_heap.dat");

        int n;
        int bubble_values[10];
        int quick_values[10];
        int heap_values[10];
        double m, sd;

        while(results >> n) {
            for(int i = 0; i < 10; i++) {
                results >> bubble_values[i] >> quick_values[i] >> heap_values[i];
            }

            m = mean(bubble_values);
            sd = deviation(bubble_values, m);
            statistics_bubble << n << " " << m << " " << sd << endl;

            m = mean(quick_values);
            sd = deviation(quick_values, m);
            statistics_quick <<  n << " " << m << " " << sd << endl;

            m = mean(heap_values);
            sd = deviation(heap_values, m);
            statistics_heap <<  n << " " << m << " " << sd << endl;
        }

        statistics_bubble.close();
        statistics_quick.close();
        statistics_heap.close();
        results.close();
    }
}
//------------------------------------------------------------
int main(){

	time_test();
    clock_test();

    calculate_statistics("time_test", "time_stat");
    calculate_statistics("clock_test", "clock_stat");

	return 0;
}
